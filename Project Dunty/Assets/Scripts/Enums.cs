﻿using System;

[Serializable]
public enum EffectType
{
    Direct,
    Passive,
    Options
}

[Serializable]
public enum RoomType
{
    NullRoom,
    FightRoom,
    HealRoom,
    BossRoom
}
[Serializable]
 public enum EffectSpecificType
 {
     Healing,
     Damage
 }

