﻿using Controllers;
using UnityEngine;

public class SettingsHandler : MonoBehaviour
{
    private GameController _gameController;

    private void Start()
    {
        _gameController = GameController.GetGameController();
    }

    public void Close()
    {
        _gameController.UnloadSettings();
    }

    public void MainMenu()
    {
        _gameController.LoadMainMenu();
    }

    public void Quit()
    {
        _gameController.Quit();
    }
}
