﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class BackGround : MonoBehaviour
{
    public Image image;
    public Sprite[] sprites;


    private void Start()
    {
        if (DateTime.Now.Hour >= 18f || DateTime.Now.Hour <= 5f)
        {
            image.sprite = sprites[0];
        }
        else
        {
            image.sprite = sprites[1];
        }
            
    }
}
