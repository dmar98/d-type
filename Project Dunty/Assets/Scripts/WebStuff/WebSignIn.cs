﻿using System.Collections;
using Controllers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace WebStuff
{
    public abstract class WebSignIn : MonoBehaviour
    {
        public TMP_InputField nameField;
        public TMP_InputField passwordField;

        public Button submitButton;

        protected GameController gameController;

        private void Start()
        {
            gameController = GameController.GetGameController();
        }

        public void CallCoroutine()
        {
            StartCoroutine(WebCall());
        }

        protected abstract IEnumerator WebCall();
    
        public void VerifyInputs()
        {
            submitButton.interactable = nameField.text.Length >= GameController.UsernameMinLength &&
                                        passwordField.text.Length >= GameController.PasswordMinLength;
        }
    
        public void Back()
        {
            gameController.LoadMainMenu();
        }
    }
}