﻿using System.Collections;
using Controllers;
using PlayerAndEnemy;
using UnityEngine;
using UnityEngine.Networking;

namespace WebStuff
{
    public class LogIn : WebSignIn
    {
        protected override IEnumerator WebCall()
        {
            JsonUser user = new JsonUser {name = nameField.text, password = passwordField.text};

            string bodyData = JsonUtility.ToJson(user, true);
            byte[] postData = System.Text.Encoding.UTF8.GetBytes(bodyData);

            UnityWebRequest request = UnityWebRequest.Post(GameController.Url + "login", UnityWebRequest.kHttpVerbPOST);

            UploadHandlerRaw uH = new UploadHandlerRaw(postData);
            request.uploadHandler = uH;
            request.SetRequestHeader("Content-Type", "application/json");

            yield return request.SendWebRequest();

            switch (request.responseCode)
            {
                case 200:
                    gameController.Player.Token = request.downloadHandler.text;
                    gameController.Player.Name = nameField.text;
                    gameController.LoadMainMenu();
                    break;
                case 409:
                    //Prompt false code or user name
                    break;
                default:
                    Debug.Log("User login failed. Error #" + request.responseCode + ":  " +
                              request.downloadHandler.text);
                    break;
            }
        }
    }
}