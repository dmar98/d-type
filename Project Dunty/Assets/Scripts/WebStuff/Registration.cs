﻿using System.Collections;
using System.Text;
using Controllers;
using PlayerAndEnemy;
using UnityEngine;
using UnityEngine.Networking;

namespace WebStuff
{
    public class Registration : WebSignIn
    {
        protected override IEnumerator WebCall()
        {
            JsonUser user = new JsonUser {name = nameField.text, password = passwordField.text};

            string bodyData = JsonUtility.ToJson(user, true);
            byte[] postData = Encoding.UTF8.GetBytes(bodyData);

            UnityWebRequest request = UnityWebRequest.Post(GameController.Url + "register", UnityWebRequest.kHttpVerbPOST);
            UploadHandlerRaw uH = new UploadHandlerRaw(postData);
            request.uploadHandler = uH;
            request.SetRequestHeader("Content-Type", "application/json");
            yield return request.SendWebRequest();

            if (request.responseCode == 200)
            {
                gameController.Player.Token = request.downloadHandler.text;
                gameController.Player.Name = nameField.text;
                gameController.LoadMainMenu();
            }
            else
            {
                Debug.Log("User creation failed. Error #" + request.responseCode + ":  " +
                          request.downloadHandler.text);
            }
        }

    }
}