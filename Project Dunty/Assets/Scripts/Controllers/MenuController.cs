﻿using TMPro;
using UnityEngine;

namespace Controllers
{
    public class MenuController : MonoBehaviour
    {
        private GameController _gameController;
        public GameObject music;
        
        public TextMeshProUGUI loginText;

        private bool _isRegister;
        
        private void Start()
        {
            _gameController = GameController.GetGameController();
            SetLoginText();
            
            
            GameObject[] objs = GameObject.FindGameObjectsWithTag("music");

            if (objs.Length > 1)
            {
                Destroy(music);
            }

            DontDestroyOnLoad(music);
        }

        private void SetLoginText()
        {
            if (_gameController.Player.Name != null)
            {
                loginText.text = "Logged in as " + _gameController.Player.Name;
            }
        }

        public void GoToMapScreen()
        {
            _gameController.LoadMap();
        }

        public void GoToRegisterScreen()
        {
            _gameController.LoadRegisterScreen();
        }
        
        public void GoToLoginScreen()
        {
            _gameController.LoadLoginScreen();
        }

        public void Quit()
        {
            _gameController.Quit();
        }
    }
}