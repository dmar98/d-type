﻿using System;
using System.Collections.Generic;
using System.Linq;
using PlayerAndEnemy;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace Controllers
{
    public class GameController
    {
        #region Singelton

        private static GameController _gameController;

        public static GameController GetGameController()
        {
            return _gameController ?? (_gameController = new GameController());
        }

        private GameController()
        {
            TextController = new TextController();
            ItemController = new ItemController(this);
            Player = new Player(this);
        }

        #endregion

        #region GlobalVars

        public const int UsernameMinLength = 4;
        public const int PasswordMinLength = 4;
        public const string Url = "http://192.168.132.211:80/";
        public bool isDamagable = true;

        #endregion

        #region Controllers

        public FightController FightController { get; private set; }
        public MapController MapController { get; private set; }
        private WebSaveAndLoadHandler WebSaveAndLoadHandler { get; set; }
        public TextController TextController { get; }
        private ItemController ItemController { get; }
        private GameView GameView { get; set; }

        #endregion

        #region Variables

        public RoomType Type { get; set; }
        private List<ItemClass> _items;
        public int CurrentLevel { get; set; }
        public Player Player { get; }

        #endregion

        #region LoadSceneController

        public void LoadMainMenu()
        {
            SceneManager.LoadScene(0);
        }

        public void LoadFightScene()
        {
            SceneManager.LoadScene(4, LoadSceneMode.Additive);
        }

        public void LoadMap()
        {
            SceneManager.LoadScene(3);
        }

        public void LoadRegisterScreen()
        {
            SceneManager.LoadScene(1);
        }

        public void LoadLoginScreen()
        {
            SceneManager.LoadScene(2);
        }

        private void UnloadFightScene()
        {
            SceneManager.UnloadSceneAsync(4);
        }

        public void LoadSettingsMenu()
        {
            SceneManager.LoadScene(5, LoadSceneMode.Additive);
        }

        public void UnloadSettings()
        {
            SceneManager.UnloadSceneAsync(5);
        }

        public void Quit()
        {
            Application.Quit();
        }

        #endregion

        #region SetControllers

        public void SetFightController(FightController fightController)
        {
            FightController = fightController;
        }

        public void SetMapController(MapController mapController)
        {
            MapController = mapController;
        }

        public void LoadItemList(List<ItemClass> items)
        {
            _items = items;
        }

        public void SetGameView(GameView gameView)
        {
            GameView = gameView;
        }

        public void SetWebSlHandler(WebSaveAndLoadHandler webSaveAndLoadHandler)
        {
            WebSaveAndLoadHandler = webSaveAndLoadHandler;
        }

        #endregion

        #region Save

        public void HandleServerData(JsonSave save)
        {
            Player.Health = save.health == 0 ? 50 : save.health;
            Player.Inventory = new Inventory();
            foreach (ItemList item in save.items)
            {
                foreach (ItemClass itemClass in _items.Where(itemClass => item.itemName == itemClass.itemName))
                {
                    Player.AddItem(itemClass);
                }
            }
        }

        public ItemList[] GetItems()
        {
            ItemList[] lists = new ItemList[Player.Inventory.Length];
            int i = 0;
            foreach (var list in Player.Inventory.ItemClasses.Select(itemClass =>
                new ItemList {itemName = itemClass.itemName}))
            {
                lists[i++] = list;
            }

            return lists;
        }

        public void SaveGame()
        {
            WebSaveAndLoadHandler.SaveGame();
        }

        public void LoadGame()
        {
            WebSaveAndLoadHandler.LoadFromDataBase();
        }

        #endregion

        #region InventoryManagment

        public void AddItem(ItemClass item)
        {
            Player.AddItem(item);
            if (CurrentLevel == 4)
            {
                MapController.ReloadNewMap();
            }
        }


        public void RemoveItem(ItemClass item)
        {
            Player.RemoveItem(item);
            UpdateInventory();
        }

        public void UseItem(ItemClass item)
        {
            ItemController.Use(item);
        }

        public void UpdateInventory()
        {
            GameView.UpdateInventory();
        }

        #endregion

        #region EntityManagment

        public void EnemyKilled()
        {
            UnloadFightScene();
            SaveGame();
            if (Type != RoomType.BossRoom) return;
            ItemClass[] classes = new ItemClass[3];
            for (int i = 0; i < classes.Length; i++)
            {
                classes[i] = _items[Random.Range(0, _items.Count)];
            }

            MapController.EnableItemSelection(classes);
        }

        public void PlayerDied()
        {
            UnloadFightScene();
            Player.Health = 50;
            Player.Inventory = new Inventory();
            MapController.ReloadNewMap();
            SaveGame();
            LoadMainMenu();
        }

        public void HealPlayer(int amount)
        {
            Player.AddHealth(amount);
        }

        public void ApplyDamage(string word, IDamageable damageable)
        {
            if (damageable == Player && isDamagable)
            {
                damageable.TakeDamage(Math.Min(TextController.CalculateDamage(word), 6));
                ShowDamaged(damageable);
            }
            else if (damageable != Player)
            {
                damageable.TakeDamage(TextController.CalculateDamage(word));
                ShowDamaged(damageable);
            }
        }

        private void ShowDamaged(IDamageable damageable)
        {
            if (damageable == Player)
            {
                GameView.DamagedPlayer();
            }
            else
            {
                GameView.EnemyDamaged();
            }
        }

        public void ApplyDamage(int amount, IDamageable damageable)
        {
            if (damageable == Player && isDamagable)
            {
                damageable.TakeDamage(amount);
                ShowDamaged(damageable);
            }
            else if (damageable != Player)
            {
                damageable.TakeDamage(amount);
                ShowDamaged(damageable);
            }
        }

        #endregion

        public void PassiveHealing(int amount, int duration)
        {
            GameView.PassiveHealing(amount, duration);
        }

        public void ApplyDamageOverTime(int amount, int duration, Enemy fightControllerEnemy)
        {
            GameView.ApplyDamageOverTime(amount, duration, fightControllerEnemy);
        }

        public void NoDamage(int effectDuration)
        {
            GameView.NoDamage(effectDuration);
        }

        public void AddLetters(int effectAmount)
        {
            GameView.AddLetters(effectAmount);
        }

        public void SlowTime(int effectDuration)
        {
            GameView.SlowTime(effectDuration);
        }
    }
}