﻿using System;

namespace Controllers
{
    public class ItemController
    {
        private readonly GameController _gameController;

        public ItemController(GameController gameController)
        {
            _gameController = gameController;
        }

        public void Use(ItemClass item)
        {
            switch (item.effect.effectType)
            {
                case EffectType.Direct:
                    UseDirectItem(item);
                    break;
                case EffectType.Passive:
                    UsePassiveItem(item);
                    break;
                case EffectType.Options:
                    UseOptionsItem(item);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            _gameController.RemoveItem(item);
            _gameController.UpdateInventory();
        }

        private void UseOptionsItem(ItemClass item)
        {
            switch (item.effect.name)
            {
                case "NoDamage":
                    _gameController.NoDamage(item.effect.duration);
                    break;
                case "MoreLetters":
                    _gameController.AddLetters(item.effect.amount);
                    break;
                case "SlowTime":
                    _gameController.SlowTime(item.effect.duration);
                    break;
            }
        }
        
        private void UsePassiveItem(ItemClass item)
        {
            switch (item.effect.effectSpecificType)
            {
                case EffectSpecificType.Healing:
                    _gameController.PassiveHealing(item.effect.amount, item.effect.duration);
                    break;
                case EffectSpecificType.Damage:
                    _gameController.ApplyDamageOverTime(item.effect.amount, item.effect.duration,_gameController.FightController.Enemy);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        
        private void UseDirectItem(ItemClass item)
        {
            switch (item.effect.effectSpecificType)
            {
                case EffectSpecificType.Healing:
                    _gameController.HealPlayer(item.effect.amount);
                    break;
                case EffectSpecificType.Damage:
                    _gameController.ApplyDamage(item.effect.amount, _gameController.FightController.Enemy);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

    }
}
