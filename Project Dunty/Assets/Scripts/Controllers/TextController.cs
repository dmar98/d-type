﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using PlayerAndEnemy;
using UnityEngine;

namespace Controllers
{
    public class TextController
    {
        public List<string> Word { get; }
        public List<string> Speech { get; }

        public TextController()
        {
            TextAsset load = Resources.Load<TextAsset>("englishWords");
            Word = ReadFromFile(load);

            load = Resources.Load<TextAsset>("Speech");
            Speech = ReadFromFile(load);
        }

        public bool IsWordLongEnough(string word, Enemy enemy)
        {
            return enemy.GetImmuneAgainstWordsLength() >= word.Length;
        }

        public bool IsWordInDic(string word)
        {
            return Word.Contains(word);
        }

        public bool IsWordEmpty(string word)
        {
            return word == "";
        }

        public int CalculateDamage(string word)
        {
            int damage = 0;

            damage = WordLengthDamage(word, damage);

            int vowelCount = VowelCount(word);

            return damage + vowelCount;
        }

        private static int WordLengthDamage(string word, int damage)
        {
            for (int i = 0; i < word.Length; i++)
            {
                if (i < 4)
                {
                    damage++;
                }
                else if (i < 6)
                {
                    damage += 2;
                }
                else
                {
                    damage += 3;
                }
            }
            return damage;
        }

        private List<string> ReadFromFile(TextAsset load)
        {
            if (load == null) return null;
            StreamReader reader = new StreamReader(new MemoryStream(load.bytes));
            List<string> texts = new List<string>();
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                texts.Add(line);
            }

            reader.Close();
            return texts;
        }

        private int VowelCount(string word)
        {
            int vowelCount = word.Count(IsVowel);
            return vowelCount;
        }

        private bool IsVowel(char t)
        {
            return t.Equals('a') || t.Equals('e') || t.Equals('i') || t.Equals('o') || t.Equals('u') || t.Equals('y');
        }
    }
}