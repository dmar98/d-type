﻿using System.Collections;
using System.Text;
using Map;
using Newtonsoft.Json;
using PlayerAndEnemy;
using UnityEngine;
using UnityEngine.Networking;

namespace Controllers
{
    public class WebSaveAndLoadHandler : MonoBehaviour
    {
        private GameController _gameController;

        private void Awake()
        {
            _gameController = GameController.GetGameController();
            _gameController.SetWebSlHandler(this);
        }

        public void SaveGame()
        {
            if (_gameController.Player.Token != null)
            {
                StartCoroutine(CallSaveGame());
            }
        }

        public void LoadFromDataBase()
        {
            if (_gameController.Player.Token != null)
            {
                StartCoroutine(LoadSave());
            }
        }

        private IEnumerator CallSaveGame()
        {
            JsonSave save = new JsonSave
            {
                health = _gameController.Player.Health,
                items = _gameController.GetItems(),
                map = new JMap {grid = GetJRooms()}
            };

            string data = JsonConvert.SerializeObject(save);
            byte[] bytes = Encoding.ASCII.GetBytes(data);

            UnityWebRequest request =
                UnityWebRequest.Post(GameController.Url + "saveData", UnityWebRequest.kHttpVerbPOST);

            UploadHandlerRaw uH = new UploadHandlerRaw(bytes);
            request.uploadHandler = uH;
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("Authorization", _gameController.Player.Token);

            yield return request.SendWebRequest();

            if (request.responseCode == 200)
            {
                
            }
            else
            {
                Debug.Log("Sending Data Failed. Error #" + request.responseCode + ":  " +
                          request.downloadHandler.text);
            }
        }

        private IEnumerator LoadSave()
        {
            UnityWebRequest request = UnityWebRequest.Get(GameController.Url + "saveData");
            request.SetRequestHeader("Authorization", _gameController.Player.Token);

            yield return request.SendWebRequest();

            if (request.responseCode == 200)
            {
                string saveData = request.downloadHandler.text;
                JsonSave serverData = JsonConvert.DeserializeObject<JsonSave>(saveData);

                if (serverData == null)
                {
                    yield break;
                }

                _gameController.HandleServerData(serverData);

                GetLoadedMap(serverData.map);
            }
            else
            {
                Debug.Log("Retrieving Data Failed. Error #" + request.responseCode + ":  " +
                          request.downloadHandler.text);
            }
        }

        private JRoom[][] GetJRooms()
        {
            JRoom[][] jRooms = new JRoom[_gameController.MapController.Map.Grid.Length][];
            for (int i = 0; i < jRooms.Length; i++)
            {
                jRooms[i] = new JRoom[_gameController.MapController.Map.Grid[i].Length];
                for (int j = 0; j < jRooms[i].Length; j++)
                {
                    jRooms[i][j] = new JRoom
                    {
                        roomType = _gameController.MapController.Map.Grid[i][j].GetRoomType(),
                        isPlayable = _gameController.MapController.Map.Grid[i][j].GetPlayable()
                    };
                }
            }

            return jRooms;
        }

        private void GetLoadedMap(JMap jMap)
        {
            if (jMap.grid.Length == 0) return;
            _gameController.MapController.RemoveOldMap();
            _gameController.MapController.Map.RecreatedGridFromSave(jMap);
            _gameController.MapController.Map.RecreatedPathWays();
            _gameController.MapController.CreateIcons();
            _gameController.MapController.Map.SetIntractable();
            _gameController.MapController.DrawLines();
        }
    }
}