﻿using PlayerAndEnemy;
using Random = UnityEngine.Random;

namespace Controllers
{
    public class FightController
    {
        public Enemy Enemy { get; }
        private readonly GameController _gameController;

        public FightController()
        {
            _gameController = GameController.GetGameController();
            Enemy = new Enemy(_gameController.CurrentLevel);
        }
        
        public void Attack()
        {
            float value = Random.value;

            if (!(value > Enemy.Difficulty)) return;
            string s = _gameController.TextController.Word[Random.Range(0, _gameController.TextController.Word.Count)];
            _gameController.ApplyDamage(s, _gameController.Player);
        }
    }
}