﻿using System;
using System.Collections.Generic;
using Map;
using UnityEngine;
using UnityEngine.UI;

namespace Controllers
{
    public class MapController : MonoBehaviour
    {
        private GameController _gameController;

        public GameObject settingsPanel;

        public DungeonMap Map { get; private set; }

        public GameObject mapIcon;
        public GameObject line;
        public Sprite[] sprite;

        public List<ItemClass> items;

        private int _currentLevel;
        public GameObject itemsPanel;
        private GameObject _mapObject;

        private void Start()
        {
            _gameController = GameController.GetGameController();
            _gameController.SetMapController(this);
            
            _gameController.LoadItemList(items);
            _mapObject = GameObject.Find("Map");
            Init();
            _gameController.LoadGame();
        }

        private void Init()
        {
            Map = new DungeonMap();
            CreateIcons();
            DrawLines();
        }

        #region ButtonDisplay

        public void ReturnToMap()
        {
            settingsPanel.SetActive(false);
        }

        public void ReturnToMainMenu()
        {
            _gameController.LoadMainMenu();
        }

        public void OpenSettingsPanel()
        {
            settingsPanel.SetActive(true);
        }

        #endregion

        #region MapDrawing

        public void CreateIcons()
        {
            for (int i = 0; i < Map.Grid.Length; i++)
            {
                for (int j = 0; j < Map.Grid[i].Length; j++)
                {
                    switch (Map.Grid[i][j].GetRoomType())
                    {
                        case RoomType.NullRoom:
                            InstantiateIcon(0, i, j);
                            break;
                        case RoomType.FightRoom:
                            InstantiateIcon(1, i, j);
                            break;
                        case RoomType.HealRoom:
                            InstantiateIcon(2, i, j);
                            break;
                        case RoomType.BossRoom:
                            InstantiateIcon(3, i, j);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
        }

        private void InstantiateIcon(int spriteIndex, int row, int col)
        {
            GameObject uiIcon = Instantiate(mapIcon, _mapObject.transform, true);
            uiIcon.GetComponent<Image>().sprite = sprite[spriteIndex];
            int gridLength = (col + 1) * Screen.width / (Map.Grid[row].Length + 1);
            int height = (row + 1) * Screen.height / (Map.Grid.Length + 1);
            uiIcon.transform.position = new Vector3(gridLength, height, 0);
            uiIcon.GetComponent<RectTransform>().sizeDelta = new Vector2(50, 50);
            Button button = uiIcon.GetComponent<Button>();

            button.onClick.AddListener(delegate { Clicked(row, col); });

           
            uiIcon.GetComponent<Button>().interactable = Map.Grid[row][col].GetPlayable();

            Map.Grid[row][col].SetIcon(uiIcon);
        }

        private void Clicked(int row, int col)
        {
            _gameController.SaveGame();
            Map.DisableRow(row);
            Map.EnableNextRow(row, col);

            _gameController.CurrentLevel = _currentLevel++;
            Map.CheckWhichRoom(row, col, _gameController);
        }

        #region DrawLines

        private void DrawLine(Vector3 pointA, Vector3 pointB)
        {
            GameObject uiLine = Instantiate(line, _mapObject.transform.GetChild(1), true);

            Vector3 differenceVector = pointB - pointA;

            uiLine.GetComponent<RectTransform>().sizeDelta = new Vector2(differenceVector.magnitude, 10);
            uiLine.GetComponent<RectTransform>().pivot = new Vector2(0, 0.5f);
            uiLine.GetComponent<RectTransform>().position = pointA;
            float angle = Mathf.Atan2(differenceVector.y, differenceVector.x) * Mathf.Rad2Deg;
            uiLine.GetComponent<RectTransform>().rotation = Quaternion.Euler(0, 0, angle);
        }

        public void DrawLines()
        {
            foreach (Room[] t1 in Map.Grid)
            {
                foreach (var t2 in t1)
                {
                    IEnumerable<Room> mapNodes = t2.GetWaysOut();
                    foreach (Room t in mapNodes)
                    {
                        DrawLine(t2.GetIcon().transform.position, t.GetIcon().transform.position);
                    }
                }
            }
        }

        #endregion

        #endregion

        public void EnableItemSelection(ItemClass[] item)
        {
            itemsPanel.SetActive(true);
            for (int i = 0; i < 3; i++)
            {
                itemsPanel.transform.GetChild(i).gameObject.GetComponent<ItemSelection>().item = item[i];
            }
        }
        public void ReloadNewMap()
        {
            RemoveOldMap();
            Map = new DungeonMap();
            CreateIcons();
            DrawLines();
        }

        public void RemoveOldMap()
        {
            foreach (Transform mapTransform in _mapObject.transform)
            {
                switch (mapTransform.gameObject.name)
                {
                    case "Room(Clone)":
                        Destroy(mapTransform.gameObject);
                        break;
                    case "Lines":
                    {
                        foreach (Transform lineTransform in mapTransform.transform)
                        {
                            Destroy(lineTransform.gameObject);
                        }
                        break;
                    }
                }
            }
        }
    }
}