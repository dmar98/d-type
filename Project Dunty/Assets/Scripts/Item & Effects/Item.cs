﻿using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour
{
    public ItemClass item;

    private Image _image;
    private Button _button;

    private GameObject _tooltipPanel;

    public GameObject tooltip;

    public void Start()
    {
        Init();
    }

    private void Init()
    {
        _image = GetComponent<Image>();
        _button = GetComponent<Button>();
        _tooltipPanel = GameObject.Find("TooltipPanel");


        _button.onClick.AddListener(Clicked);

        _image.sprite = item.sprite;
    }

    private void Clicked()
    {
        if (_tooltipPanel.transform.childCount > 0)
        {
            Destroy(_tooltipPanel.transform.GetChild(0).gameObject);
        }
        GameObject tip = Instantiate(tooltip, _tooltipPanel.transform);
        tip.GetComponent<ToolTip>().SetItem(item);
    }
}
