﻿#if UNITY_EDITOR
#endif
using UnityEngine;

[CreateAssetMenu(fileName = "Effect", menuName = "Effect")]
public class Effect : ScriptableObject
{
    public EffectType effectType;
    public EffectSpecificType effectSpecificType;

    public int duration;
    public int amount;
}

