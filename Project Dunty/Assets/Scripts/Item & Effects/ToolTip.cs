﻿using Controllers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ToolTip : MonoBehaviour
{
    private float _timeToLive = 5;

    public Button use;
    public Button discard;
    
    public TextMeshProUGUI description;
    public TextMeshProUGUI itemName;
    
    

    private GameController _gameController;

    private ItemClass Item { get; set; }

    private void Start()
    {
        _gameController = GameController.GetGameController();
        use.onClick.AddListener(ClickedUse);
        discard.onClick.AddListener(ClickedDiscard);

        
    }

    private void ClickedDiscard()
    {
        _gameController.RemoveItem(Item);
        Destroy(gameObject);
    }

    private void ClickedUse()
    {
        _gameController.UseItem(Item);
        Destroy(gameObject);
    }

    private void Update()
    {
        _timeToLive -= Time.deltaTime;

        if (_timeToLive < 0)
        {
            Destroy(gameObject);
        }
    }

    public void SetItem(ItemClass item)
    {
        Item = item;
        
        description.text = Item.description;
        itemName.text = Item.itemName;
    }
}
