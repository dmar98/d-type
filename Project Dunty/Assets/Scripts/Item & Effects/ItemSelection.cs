﻿using Controllers;
using UnityEngine;
using UnityEngine.UI;

public class ItemSelection : MonoBehaviour
{
    public ItemClass item;
    private Image _image;
    private Button _button;

    private GameController _gameController;
    
    public void Start()
    {
        Init();
    }

    private void Init()
    {
        _gameController = GameController.GetGameController();
        _image = GetComponent<Image>();
        _button = GetComponent<Button>();


        _button.onClick.AddListener(Clicked);

        _image.sprite = item.sprite;
    }

    private void Clicked()
    {
        _gameController.AddItem(item);
        transform.parent.gameObject.SetActive(false);
        _gameController.SaveGame();
    }
}
