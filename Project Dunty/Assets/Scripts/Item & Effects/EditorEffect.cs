﻿using System;
using UnityEditor;

#if UNITY_EDITOR
[CustomEditor(typeof(Effect)), CanEditMultipleObjects]
public class EditorEffect : Editor
{
    public SerializedProperty
        effectTypeProp,
        durationProp,
        amountProp,
        effectSpecificTypeProp;

    private void OnEnable()
    {
        effectTypeProp = serializedObject.FindProperty("effectType");
        durationProp = serializedObject.FindProperty("duration");
        amountProp = serializedObject.FindProperty("amount");
        effectSpecificTypeProp = serializedObject.FindProperty("effectSpecificType");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(effectTypeProp);
        EditorGUILayout.PropertyField(amountProp);


        EffectType effectType = (EffectType) effectTypeProp.enumValueIndex;

        switch (effectType)
        {
            case EffectType.Direct:
                EditorGUILayout.PropertyField(effectSpecificTypeProp);
                break;
            case EffectType.Passive:
                EditorGUILayout.PropertyField(effectSpecificTypeProp);
                EditorGUILayout.PropertyField(durationProp);
                break;
            case EffectType.Options:
                EditorGUILayout.PropertyField(durationProp);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        serializedObject.ApplyModifiedProperties();
    }
}
#endif