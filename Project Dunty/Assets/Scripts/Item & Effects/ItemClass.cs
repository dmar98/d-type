﻿using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "Item")]
public class ItemClass : ScriptableObject
{
    public string itemName;
    public Effect effect;
    public string description;
    public Sprite sprite;
}