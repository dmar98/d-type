﻿using System.Collections;
using ButtonsAndLetters;
using Controllers;
using PlayerAndEnemy;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameView : MonoBehaviour
{
    #region Variables

    private GameController _gameController;
    private FightController _fightController;

    private float _timer;
    [SerializeField] private float timer = 20;

    public GameObject[] inventoryItems;

    public GameObject buttonPanel;
    [SerializeField] private int numberOfButtons = 16;
    private bool _isButtonSizeChanged;

    private WordUpdater _wordUpdater;

    public GameObject playerDamage;
    public GameObject enemyDamage;
    private GameObject _music;
    
    #endregion

    #region Enemy Speech Variables

    public GameObject speech;
    private TextMeshProUGUI _bubble;
    private float _speechTimer;
    [SerializeField] private float speechTimer = 4;

    #endregion

    #region Unity Methodes

    private void Awake()
    {
        _gameController = GameController.GetGameController();
        buttonPanel.GetComponent<SpawnButtons>().SetUpButtons(numberOfButtons);
    }

    private void Start()
    {
        _fightController = new FightController();
        _gameController.SetFightController(_fightController);

        _gameController.SetGameView(this);

        ButtonInit();

        _wordUpdater = GameObject.Find("Center").transform.GetComponent<WordUpdater>();

        _bubble = speech.transform.GetChild(0).GetComponent<TextMeshProUGUI>();

        if (_gameController.Player.Inventory == null)
        {
            _gameController.Player.Inventory = new Inventory();
        }

        for (int i = 0; i < _gameController.Player.Inventory.Length; i++)
        {
            foreach (var inventoryItem in inventoryItems)
            {
                if (inventoryItem.activeSelf) continue;
                inventoryItem.SetActive(true);
                inventoryItem.GetComponent<Item>().item = _gameController.Player.Inventory.ItemClasses[i];
                break;
            }
        }

        _timer = timer;
        _music = GameObject.FindGameObjectWithTag("music");
    }

    private void Update()
    {
        if (_speechTimer > 0)
        {
            _speechTimer -= Time.deltaTime;
        }
        else
        {
            speech.SetActive(false);
        }

        if (_timer > 0)
        {
            _timer -= Time.deltaTime;
        }
        else
        {
            ClearBoard();
            _gameController.ApplyDamage(5, _gameController.Player);
            _timer = timer;
        }

        _music.GetComponent<AudioSource>().pitch = _timer < 5 ? 1.2f : 1;
    }

    #endregion

    #region ButtonClicks

    private void ButtonInit()
    {
        foreach (Transform button in buttonPanel.transform)
        {
            button.GetComponent<Button>().onClick.AddListener(
                delegate { Clicked(button.GetComponent<Button>()); });
        }
    }

    private void Clicked(Selectable buttonLocal)
    {
        buttonLocal.interactable = false;
        _wordUpdater.UpdateWord(buttonLocal.GetComponent<RandomLetter>().Text.text);
    }

    public void ClickedSettings()
    {
        _gameController.LoadSettingsMenu();
    }

    public void ClickedEnter()
    {
        string word = _wordUpdater.GetWord();
        _timer = timer;
        if (_gameController.TextController.IsWordEmpty(word))
        {
            _gameController.Player.TakeDamage(10);
            SetSpeechBubble(1);
            return;
        }

        CheckWord(word.ToLower());

        _fightController.Attack();
    }

    public void ClickedDelete()
    {
        foreach (Transform button in buttonPanel.transform)
        {
            button.GetComponent<Button>().interactable = true;
        }

        _wordUpdater.DeleteWord();
    }

    #endregion

    #region VisualUpdater

    private void ClearBoard()
    {
        ClickedDelete();
        ChangeLetters();
    }

    private void ChangeLetters()
    {
        foreach (Transform button in buttonPanel.transform)
        {
            button.GetComponent<RandomLetter>().Text.text = RandomLetter.GetRandomLetter();
        }
    }

    public void UpdateInventory()
    {
        foreach (var inventoryItem in inventoryItems)
        {
            inventoryItem.SetActive(false);
        }

        for (int i = 0; i < _gameController.Player.Inventory.Length; i++)
        {
            foreach (var inventoryItem in inventoryItems)
            {
                if (inventoryItem.activeSelf) continue;
                inventoryItem.SetActive(true);
                inventoryItem.GetComponent<Item>().item = _gameController.Player.Inventory.ItemClasses[i];
                break;
            }
        }
    }

    private void SetSpeechBubble(int i)
    {
        speech.SetActive(true);
        _bubble.text = _gameController.TextController.Speech[i];
        _speechTimer = speechTimer;
    }

    private void CheckWord(string word)
    {
        if (_gameController.TextController.IsWordLongEnough(word, _gameController.FightController.Enemy))
        {
            SetSpeechBubble(0);
            ClickedDelete();
            return;
        }

        if (_gameController.TextController.IsWordInDic(word))
        {
            _gameController.ApplyDamage(word, _gameController.FightController.Enemy);
            ClearBoard();
            if (!_isButtonSizeChanged) return;
            DeleteButtons();
            buttonPanel.GetComponent<SpawnButtons>().SetUpButtons(numberOfButtons);
            _isButtonSizeChanged = false;
            ButtonInit();
        }
        else
        {
            SetSpeechBubble(2);
            ClickedDelete();
        }
    }
    
    public void DamagedPlayer()
    {
        playerDamage.SetActive(true);
        StartCoroutine(Damaged());
    }

    private IEnumerator Damaged()
    {
        yield return new WaitForSeconds(2);
        playerDamage.SetActive(false);
        enemyDamage.SetActive(false);
    }

    public void EnemyDamaged()
    {
        enemyDamage.SetActive(true);
        StartCoroutine(Damaged());
    }

    #endregion

    public void PassiveHealing(int amount, int duration)
    {
        StartCoroutine(HealPlayer(amount, duration));
    }

    private IEnumerator HealPlayer(int amount, int duration)
    {
        for (int i = 0; i < amount; i++)
        {
            yield return new WaitForSecondsRealtime(duration);
            _gameController.HealPlayer(1);
        }
    }

    public void ApplyDamageOverTime(int amount, int duration, Enemy fightControllerEnemy)
    {
        StartCoroutine(DealDamageOverTime(amount, duration, fightControllerEnemy));
    }

    private IEnumerator DealDamageOverTime(int amount, int duration, IDamageable damageable)
    {
        for (int i = 0; i < amount; i++)
        {
            yield return new WaitForSecondsRealtime(duration);
            _gameController.ApplyDamage(1, damageable);
        }
    }

    public void NoDamage(int effectDuration)
    {
        StartCoroutine(Immortal(effectDuration));
    }

    private IEnumerator Immortal(int effectDuration)
    {
        _gameController.isDamagable = false;
        yield return new WaitForSecondsRealtime(effectDuration);
        _gameController.isDamagable = true;
    }

    public void AddLetters(int effectAmount)
    {
        numberOfButtons = effectAmount;
        DeleteButtons();
        buttonPanel.GetComponent<SpawnButtons>().SetUpButtons(numberOfButtons);
        _isButtonSizeChanged = true;
        numberOfButtons = 16;
        ButtonInit();
    }

    private void DeleteButtons()
    {
        foreach (Transform button in buttonPanel.transform)
        {
            Destroy(button.gameObject);
        }
    }

    public void SlowTime(int effectDuration)
    {
        _timer = effectDuration;
    }
}