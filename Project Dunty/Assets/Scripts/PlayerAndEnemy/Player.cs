﻿using Controllers;

namespace PlayerAndEnemy
{
    public class Player : IDamageable
    {
        private readonly GameController _gameController;
        private const int MaxHealth = 50;

        public int Health { get; set; }

        public Inventory Inventory { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }

        public Player(GameController gameController)
        {
            _gameController = gameController;
            Inventory = new Inventory();
            Health = MaxHealth;
            Name = null;
        }

        public void AddHealth(int amount)
        {
            Health += amount;
            if (Health >= MaxHealth)
            {
                Health = MaxHealth;
            }
        }

        public void TakeDamage(int amount)
        {
            Health -= amount;
            if (Health > 0) return;
            _gameController.PlayerDied();
        }

        public int GetHealth()
        {
            return MaxHealth;
        }

        public int GetCurrentHealth()
        {
            return Health;
        }

        public void AddItem(ItemClass item)
        {
            Inventory.AddItem(item);
        }

        public void RemoveItem(ItemClass item)
        {
            Inventory.RemoveItem(item);
        }
    }
}