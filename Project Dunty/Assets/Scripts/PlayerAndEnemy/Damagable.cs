﻿namespace PlayerAndEnemy
{
    public interface IDamageable
    {
        void TakeDamage(int amount);
    }
}