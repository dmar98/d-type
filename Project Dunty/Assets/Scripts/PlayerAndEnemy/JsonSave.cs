﻿using Map;
using Newtonsoft.Json;

namespace PlayerAndEnemy
{
    public class JsonSave
    {
        [JsonProperty("health")] public int health;
        [JsonProperty("items")] public ItemList[] items;
        [JsonProperty("map")] public JMap map;
    }

    public class ItemList
    {
        [JsonProperty("itemName")] public string itemName;
    }
}