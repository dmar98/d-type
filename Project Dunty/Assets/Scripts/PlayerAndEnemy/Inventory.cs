﻿using System;
using System.Collections.Generic;

namespace PlayerAndEnemy
{
    [Serializable]
    public class Inventory
    {
        public int Length { get; set; }
        public List<ItemClass> ItemClasses { get; set; }

        public Inventory()
        {
            ItemClasses = new List<ItemClass>();
        }

        public void AddItem(ItemClass item)
        {
            ItemClasses.Add(item);
            Length++;
        }

        public void RemoveItem(ItemClass item)
        {
            ItemClasses.Remove(item);
            Length--;
        }
    }
}