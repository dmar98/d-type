﻿using System;
using UnityEngine;

namespace PlayerAndEnemy
{
    [Serializable]
    public class JsonUser
    {
        [SerializeField]
        public string name;

        [SerializeField]
        public string password;

    }
}