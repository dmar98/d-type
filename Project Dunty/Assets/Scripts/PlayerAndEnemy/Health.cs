﻿using Controllers;

using TMPro;
using UnityEngine;

namespace PlayerAndEnemy
{
    public class Health : MonoBehaviour
    {
        public GameObject father;

        public RectTransform lifeBar;
        public TextMeshProUGUI text;
        private GameController _gameController;

        private void Start()
        {
            _gameController = GameController.GetGameController();
        }

        private void Update()
        {
            UpdateHealthBar();
        }

        private void UpdateHealthBar()
        {
            float enemyHealth;
            if (father.name == "Player")
            {
                enemyHealth = _gameController.Player.GetCurrentHealth() / (float) _gameController.Player.GetHealth();
                if (_gameController.Player.Name != null)
                {
                    text.text = _gameController.Player.Name + ": " + _gameController.Player.GetCurrentHealth() + " / " +
                                _gameController.Player.GetHealth();
                }
                else
                {
                    text.text = "You: " + _gameController.Player.GetCurrentHealth() + " / " +
                                _gameController.Player.GetHealth();
                }
            }
            else
            {
                enemyHealth = _gameController.FightController.Enemy.GetCurrentHealth() /
                              (float) _gameController.FightController.Enemy.GetHealth();
                text.text = _gameController.FightController.Enemy.Name + ": " +
                            _gameController.FightController.Enemy.GetCurrentHealth() + " / " +
                            _gameController.FightController.Enemy.GetHealth();
            }

            lifeBar.localScale = new Vector3(enemyHealth, 1, 1);
        }
    }
}