﻿using Controllers;
using RandomNameGeneratorLibrary;

namespace PlayerAndEnemy
{
    public class Enemy : IDamageable
    {
        private int Health { get; set; }
        private int CurrentHealth { get; set; }
        public float Difficulty { get; private set; }
        public string Name { get; }

        private const int ImmuneAgainstWordsLength = 2;

        private readonly GameController _gameController;

        public Enemy(float difficulty, int health = 50)
        {
            var personGenerator = new PersonNameGenerator();
            var name = personGenerator.GenerateRandomFirstAndLastName();
            Name = name;
            Health = health;
            GetDifficulty(difficulty);
            _gameController = GameController.GetGameController();
            CurrentHealth = Health;
        }

        private void GetDifficulty(float difficulty)
        {
            switch (difficulty)
            {
                case 0:
                    Difficulty = 0.9f;
                    Health += -10;
                    break;
                case 1:
                    Difficulty = 0.85f;
                    Health += -5;
                    break;
                case 2:
                    Difficulty = 0.8f;
                    Health += 0;
                    break;
                case 3:
                    Difficulty = 0.75f;
                    Health += 5;
                    break;
                case 4:
                    Difficulty = 0.7f;
                    Health += 10;
                    break;
            }
        }

        public int GetImmuneAgainstWordsLength()
        {
            return ImmuneAgainstWordsLength;
        }
        public void TakeDamage(int damage)
        {
            CurrentHealth -= damage;
            if (CurrentHealth <= 0)
            {
                Die();
            }
        }

        public int GetHealth()
        {
            return Health;
        }

        public int GetCurrentHealth()
        {
            return CurrentHealth;
        }

        private void Die()
        {
            _gameController.EnemyKilled();
        }
    }
}