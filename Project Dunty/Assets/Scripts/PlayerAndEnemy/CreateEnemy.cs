﻿using UnityEngine;
using UnityEngine.UI;

namespace PlayerAndEnemy
{
    public class CreateEnemy : MonoBehaviour
    {
        public Image bodyImage;
        public Image faceImage;
        public Image hairImage;
        public Image clothesImage;
        public Sprite[] bodySprites;
        public Sprite[] faceSprites;
        public Sprite[] hairSprites;
        public Sprite[] clothesSprites;

    
        private void Start () {
            RandomizeCharacter();
        }

        private void RandomizeCharacter(){
            bodyImage.sprite = bodySprites[Random.Range(0,bodySprites.Length)];
            faceImage.sprite = faceSprites[Random.Range(0,faceSprites.Length)];
            hairImage.sprite = hairSprites[Random.Range(0,hairSprites.Length)];
            clothesImage.sprite = clothesSprites[Random.Range(0,clothesSprites.Length)];
        }
    }
}
