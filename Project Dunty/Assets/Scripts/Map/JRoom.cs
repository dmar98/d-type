﻿using Newtonsoft.Json;

namespace Map
{
    public class JRoom
    {
        [JsonProperty("roomType")] public RoomType roomType;
        [JsonProperty("isPlayable")] public bool isPlayable;
    }
}