﻿using System.Collections.Generic;
using Controllers;
using UnityEngine;
using UnityEngine.UI;

namespace Map
{
    public class DungeonMap
    {
        public Room[][] Grid { get; private set; }

        private const int Rows = 5;
        private const int MaxCols = 4;
        private bool _isHealRoom;
        private const int HealingRoomAmount = 10;

        public DungeonMap()
        {
            Init();
            AddWays();
        }

        private void Init()
        {
            Grid = new Room[Rows][];

            for (int i = 0; i < Grid.Length; i++)
            {
                int cols = Mathf.RoundToInt(Random.Range(1, MaxCols));
                Grid[i] = new Room[cols];
            }
        }

        private void AddWays()
        {
            for (int i = 0; i < Grid.Length; i++)
            {
                for (int j = 0; j < Grid[i].Length; j++)
                {
                    PopulateGridWithRooms(i, j);
                    if (i == 0)
                    {
                        Grid[i][j].SetPlayable(true);
                    }
                }

                if (i == 0) continue;
                {
                    for (int j = 0; j < Grid[i - 1].Length; j++)
                    {
                        if (Grid[i - 1][j].GetRoomType() == RoomType.HealRoom)
                        {
                            _isHealRoom = false;
                        }
                    }

                    for (int j = 0; j < Grid[i].Length; j++)
                    {
                        CreatePathWays(i, j);
                    }
                }
            }
        }

        public void RecreatedPathWays()
        {
            for (int i = 1; i < Grid.Length; i++)
            {
                for (int j = 0; j < Grid[i].Length; j++)
                {
                    CreatePathWays(i, j);
                }
            }
        }

        private void CreatePathWays(int i, int j)
        {
            if (Grid[i - 1].Length == 1)
            {
                Grid[i - 1][0].AddWaysOut(Grid[i][j]);
            }
            else if (Grid[i - 1].Length == Grid[i].Length)
            {
                Grid[i - 1][j].AddWaysOut(Grid[i][j]);
            }
            else if (Grid[i - 1].Length == 2)
            {
                if (Grid[i].Length == 1)
                {
                    Grid[i - 1][0].AddWaysOut(Grid[i][j]);
                    Grid[i - 1][1].AddWaysOut(Grid[i][j]);
                }
                else
                {
                    if (j != 0) return;
                    int ran = Random.Range(0, 2);
                    if (ran == 0)
                    {
                        Grid[i - 1][0].AddWaysOut(Grid[i][0]);
                        Grid[i - 1][1].AddWaysOut(Grid[i][2]);

                        Grid[i - 1][0].AddWaysOut(Grid[i][1]);
                    }
                    else
                    {
                        Grid[i - 1][0].AddWaysOut(Grid[i][0]);
                        Grid[i - 1][1].AddWaysOut(Grid[i][2]);

                        Grid[i - 1][1].AddWaysOut(Grid[i][1]);
                    }
                }
            }
            else
            {
                if (Grid[i].Length == 1)
                {
                    Grid[i - 1][0].AddWaysOut(Grid[i][j]);
                    Grid[i - 1][1].AddWaysOut(Grid[i][j]);
                    Grid[i - 1][2].AddWaysOut(Grid[i][j]);
                }
                else
                {
                    if (j != 0) return;
                    if (Random.Range(0, 2) == 0)
                    {
                        Grid[i - 1][0].AddWaysOut(Grid[i][0]);
                        Grid[i - 1][2].AddWaysOut(Grid[i][1]);

                        Grid[i - 1][1].AddWaysOut(Grid[i][0]);
                    }
                    else
                    {
                        Grid[i - 1][0].AddWaysOut(Grid[i][0]);
                        Grid[i - 1][2].AddWaysOut(Grid[i][1]);

                        Grid[i - 1][1].AddWaysOut(Grid[i][1]);
                    }
                }
            }
        }

        private void PopulateGridWithRooms(int i, int j)
        {
            if (i == 0)
            {
                Grid[i][j] = new Room(RoomType.FightRoom);
            }
            else if (i == Grid.Length - 1)
            {
                Grid[i][j] = new Room(RoomType.BossRoom);
            }
            else if (!_isHealRoom)
            {
                Grid[i][j] = new Room();
                if (Grid[i][j].GetRoomType() == RoomType.HealRoom)
                {
                    _isHealRoom = true;
                }
            }
            else
            {
                Grid[i][j] = new Room(RoomType.FightRoom);
            }
        }

        public void EnableNextRow(int row, int col)
        {
            if (row == Grid.Length - 1) return;
            IEnumerable<Room> waysOut = Grid[row][col].GetWaysOut();
            foreach (Room room in waysOut)
            {
                room.SetPlayable(true);
            }

            for (int k = 0; k < Grid[row + 1].Length; k++)
            {
                Grid[row + 1][k].GetIcon().GetComponent<Button>().interactable = Grid[row + 1][k].GetPlayable();
            }
        }

        public void DisableRow(int row)
        {
            for (int k = 0; k < Grid[row].Length; k++)
            {
                Grid[row][k].SetPlayable(false);
                Grid[row][k].GetIcon().GetComponent<Button>().interactable = Grid[row][k].GetPlayable();
            }
        }

        public void CheckWhichRoom(int row, int col, GameController gameController)
        {
            RoomType roomType = Grid[row][col].GetRoomType();
            if (roomType == RoomType.HealRoom)
            {
                gameController.HealPlayer(HealingRoomAmount);
            }
            else
            {
                gameController.LoadFightScene();
            }

            gameController.Type = roomType;
        }

        public void RecreatedGridFromSave(JMap jMap)
        {
            Grid = new Room[jMap.grid.Length][];
            for (int i = 0; i < Grid.Length; i++)
            {
                Grid[i] = new Room[jMap.grid[i].Length];
                for (int j = 0; j < Grid[i].Length; j++)
                {
                    Grid[i][j] = new Room(jMap.grid[i][j].roomType, jMap.grid[i][j].isPlayable);
                }
            }
        }

        public void SetIntractable()
        {
            foreach (Room[] room in Grid)
            {
                foreach (Room inRoom in room)
                {
                    inRoom.GetIcon().GetComponent<Button>().interactable = inRoom.GetPlayable();
                }
            }
        }
    }
}