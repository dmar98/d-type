﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Map
{
    [Serializable]
    public class Room
    {
        private readonly List<Room> _waysOut;

        private readonly RoomType _roomType;

        private GameObject _icon;

        private bool _isPlayable;

        public Room(RoomType roomType, bool isPlayable)
        {
            _roomType = roomType;
            _isPlayable = isPlayable;
            _waysOut = new List<Room>();
        }

        public Room(RoomType roomType = RoomType.NullRoom)
        {
            _roomType = roomType;
            if (_roomType == RoomType.NullRoom)
            {
                _roomType = (RoomType) Random.Range(1, 4);
            }
            _waysOut = new List<Room>();
        }

        public RoomType GetRoomType()
        {
            return _roomType;
        }
    
        public IEnumerable<Room> GetWaysOut()
        {
            return _waysOut;
        }

        public void AddWaysOut(Room room)
        {
            _waysOut.Add(room);
        }

        public void SetIcon(GameObject icon)
        {
            _icon = icon;
        }
    
        public GameObject GetIcon()
        {
            return _icon;
        }
    
    
        public void SetPlayable(bool playable)
        {
            _isPlayable = playable;
        }

        public bool GetPlayable()
        {
            return _isPlayable;
        }
    }

    
}