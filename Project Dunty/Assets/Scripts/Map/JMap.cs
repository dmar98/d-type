﻿using Newtonsoft.Json;

namespace Map
{
    public class JMap
    {
        [JsonProperty("grid")] public JRoom[][] grid;
    }
}