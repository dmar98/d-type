﻿using TMPro;
using UnityEngine;

namespace ButtonsAndLetters
{
    public class RandomLetter : MonoBehaviour
    {
        private const float PercentageOfConstants = 0.65f;
        
        public TextMeshProUGUI Text { get; private set; }

        private void Start()
        {
            Text = GetComponentInChildren<TextMeshProUGUI>();
            Text.text = GetRandomLetter();
        }

        public static string GetRandomLetter()
        {
            char[] consonants =
                {'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'};

            char[] vowels = {'a', 'e', 'i', 'o', 'u'};


            return Random.value <= PercentageOfConstants
                ? ("" + consonants[Random.Range(0, consonants.Length - 1)]).ToUpper()
                : ("" + vowels[Random.Range(0, vowels.Length - 1)]).ToUpper();
        }
        
    }
}
