﻿using TMPro;
using UnityEngine;

namespace ButtonsAndLetters
{
    public class WordUpdater : MonoBehaviour
    {
        private string _wordTyped;
        private TextMeshProUGUI _textMeshProUGui;

        private void Start()
        {
            _textMeshProUGui = GetComponentInChildren<TextMeshProUGUI>();
        }

        public void UpdateWord(string letter)
        {
            _wordTyped += letter;
            _textMeshProUGui.text = _wordTyped;
        }

        public void DeleteWord()
        {
            _wordTyped = "";
            _textMeshProUGui.text = _wordTyped;
        }

        public string GetWord()
        {
            return _wordTyped;
        }
    }
}