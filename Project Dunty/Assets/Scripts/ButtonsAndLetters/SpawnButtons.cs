﻿using UnityEngine;

namespace ButtonsAndLetters
{
    public class SpawnButtons : MonoBehaviour
    {
        public GameObject button;

        private RectTransform _rectTransform;
        private float _zeroPointX;
        private float _zeroPointY;
        private float _width;
        private float _height;

        private void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();
            Vector3 position = _rectTransform.position;
            float middleX = position.x;
            float middleY = position.y;
            Rect rect = _rectTransform.rect;
            _width = rect.size.x;
            _height = rect.size.y;
            _zeroPointX = middleX - _width / 2f;
            _zeroPointY = middleY - _height / 2f;
        }

        public void SetUpButtons(int numberOfButtons)
        {
            int root = (int) Mathf.Sqrt(numberOfButtons);
            int rest = numberOfButtons - root * root;

            if (rest == 0)
            {
                SetUpGrid(root, root);
            }
            else
            {
                int rows = GetHighestFactor(numberOfButtons, root);
                int cols = numberOfButtons / rows;

                SetUpGrid(rows, cols);
            }
        }

        private void SetUpGrid(int cols, int rows)
        {
            for (int i = 1; i <= cols; i++)
            {
                for (int j = 1; j <= rows; j++)
                {
                    GameObject buttonGameObject = Instantiate(button, transform);
                    buttonGameObject.transform.position = new Vector3(_zeroPointX + _width * i / (cols + 1),
                        _zeroPointY + _height * j / (rows + 1), 0);

                    buttonGameObject.transform.localScale /= rows + 5;
                }
            }
        }

        private static int GetHighestFactor(int numberOfButtons, int root)
        {
            for (int i = root; i > 0; i++)
            {
                if (numberOfButtons % i == 0) return i;
            }

            return 1;
        }
    }
}