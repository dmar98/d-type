﻿using UnityEngine;
using UnityEngine.UI;

namespace Map
{
    public class SpawnMap : MonoBehaviour
    {
        [SerializeField] private int rows = 3;
        [SerializeField] private int maxCols = 3;

        [SerializeField] private GameObject fight;
    
        private GameObject[][] _grid;

        public GameObject[] rooms;

        private RectTransform _rectTransform;
        private float _width;
        private float _height;
        private float _zeroPointX;
        private float _zeroPointY;

        private void Start()
        {
            PanelSizes();
            Init();
            SpawnTheMap();

            foreach (Transform button in transform)
            {
                button.GetComponent<Button>().onClick.AddListener(delegate { Clicked(button.GetComponent<Button>()); });
            }
        }

        private void PanelSizes()
        {
            _rectTransform = GetComponent<RectTransform>();
            Vector3 position = _rectTransform.position;
            float middleX = position.x;
            float middleY = position.y;
            Rect rect = _rectTransform.rect;
            _width = rect.size.x;
            _height = rect.size.y;
            _zeroPointX = middleX - _width / 2f;
            _zeroPointY = middleY - _height / 2f;
        }

        private void Init()
        {
            _grid = new GameObject[rows][];

            for (int i = 0; i < _grid.Length; i++)
            {
                int cols = Mathf.RoundToInt(Random.Range(1, maxCols));
                _grid[i] = new GameObject[cols];
            }
        }

        private void SpawnTheMap()
        {
            for (int i = 0; i < _grid.Length; i++)
            {
                for (int j = 0; j < _grid[i].Length; j++)
                {
                    int room = Mathf.RoundToInt(Random.Range(0, rooms.Length - 1));
                    _grid[i][j] = rooms[room];

                    Instantiate(rooms[room],
                        new Vector3(_zeroPointX + _width * (j + 1) / (_grid[i].Length + 1),
                            _zeroPointY + _height * (i + 1) / (_grid.Length + 1), 0),
                        Quaternion.identity, transform);
                }
            }
        }
    
        private void Clicked(Selectable buttonLocal)
        {
            buttonLocal.interactable = false;
            fight.SetActive(true);
            transform.root.gameObject.SetActive(false);
        
        }
    }
}