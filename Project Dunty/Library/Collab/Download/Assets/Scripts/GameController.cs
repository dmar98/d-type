﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using ButtonsAndLetters;
using PlayerAndEnemy;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour
{
    public Button enter;
    public Button delete;

    public GameObject buttonPanel;

    private const float PercentageOfConstants = 0.50f;

    private Enemy _enemy;
    private Player _player;


    private List<string> _words;

    private Transform _centerGameObject;

    [SerializeField] private int numberOfButtons = 16;

    #region Enemy Speech

    public GameObject speech;
    private List<string> _speechTexts;
    private TextMeshProUGUI _bubble;
    private float _speechTimer;
    [SerializeField] private float speechTimer = 4;

    #endregion


    #region Unity Methodes

    private void Awake()
    {
        buttonPanel.GetComponent<SpawnButtons>().SetUpButtons(numberOfButtons);
    }

    private void Start()
    {
        foreach (Transform button in buttonPanel.transform)
        {
            button.GetComponent<Button>().onClick.AddListener(delegate { Clicked(button.GetComponent<Button>()); });
        }

        enter.onClick.AddListener(ClickedEnter);
        delete.onClick.AddListener(ClickedDelete);

        _centerGameObject = GameObject.Find("/Canvas/WordShowPlace/Center").transform;

        _enemy = GameObject.Find("/Canvas/Dungeon/Enemy Position/Enemy").GetComponent<Enemy>();
        _player = GameObject.Find("/Canvas/Dungeon/Player").GetComponent<Player>();

        TextAsset load = Resources.Load<TextAsset>("englishWords");
        _words = ReadFromFile(load);

        load = Resources.Load<TextAsset>("Speech");
        _speechTexts = ReadFromFile(load);

        _bubble = speech.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
    }


    private void Update()
    {
        if (_speechTimer > 0)
        {
            _speechTimer -= Time.deltaTime;
        }
        else
        {
            speech.SetActive(false);
        }
    }

    #endregion

    #region ButtonClicks

    private void ClickedEnter()
    {
        string word = GetWord();
        if (word == "")
        {
            _player.TakeDamage(10);
            speech.SetActive(true);
            _bubble.text = _speechTexts[1];
            _speechTimer = speechTimer;
            return;
        }

        CheckWord(word.ToLower());
    }

    private string GetWord()
    {
        return _centerGameObject.GetComponent<WordUpdater>().GetWord();
    }

    private void ClickedDelete()
    {
        foreach (Transform button in buttonPanel.transform)
        {
            button.GetComponent<Button>().interactable = true;
        }

        _centerGameObject.GetComponent<WordUpdater>().DeleteWord();
    }

    private void Clicked(Selectable buttonLocal)
    {
        buttonLocal.interactable = false;
        _centerGameObject.GetComponent<WordUpdater>().UpdateWord(buttonLocal.GetComponent<RandomLetter>().Text.text);
    }

    #endregion

    private void CheckWord(string word)
    {
        if (_enemy.GetImmuneAgainstWordsLength() >= word.Length)
        {
            speech.SetActive(true);
            _bubble.text = _speechTexts[0];
            _speechTimer = speechTimer;
            ClickedDelete();
            return;
        }

        if (_words.Contains(word))
        {
            ApplyDamage(word);
            ClearBoard();
        }
        else
        {
            _player.TakeDamage(word.Length);
            speech.SetActive(true);
            _bubble.text = _speechTexts[2];
            _speechTimer = speechTimer;
            ClickedDelete();
        }
    }

    private void ApplyDamage(string word)
    {
        _enemy.TakeDamage(damage: CalculateDamage(word));
    }

    private void ClearBoard()
    {
        ClickedDelete();
        ChangeLetters();
    }

    private void ChangeLetters()
    {
        foreach (Transform button in buttonPanel.transform)
        {
            button.GetComponent<RandomLetter>().Text.text = GetRandomLetter();
        }
    }

    private int CalculateDamage(string word)
    {
        int vowelCount = VowelCount(word);

        return word.Length + vowelCount;
    }

    private int VowelCount(string word)
    {
        int vowelCount = word.Count(IsVowel);
        return vowelCount;
    }

    private bool IsVowel(char t)
    {
        return t.Equals('a') || t.Equals('e') || t.Equals('i') || t.Equals('o') || t.Equals('u');
    }

    private List<string> ReadFromFile(TextAsset load)
    {
        if (load == null) return null;
        StreamReader reader = new StreamReader(new MemoryStream(load.bytes));
        List<string> texts = new List<string>();
        string line;
        while ((line = reader.ReadLine()) != null)
        {
            texts.Add(line);
        }

        reader.Close();
        return texts;
    }


    public static string GetRandomLetter()
    {
        char[] consonants =
            {'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'};

        char[] vowels = {'a', 'e', 'i', 'o', 'u'};


        return Random.value <= PercentageOfConstants
            ? ("" + consonants[Random.Range(0, consonants.Length - 1)]).ToUpper()
            : ("" + vowels[Random.Range(0, vowels.Length - 1)]).ToUpper();
    }
}