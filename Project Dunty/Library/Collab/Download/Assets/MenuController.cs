﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public Button play;
    public Button options;
    public Button credits;

    private void Start()
    {
        play.onClick.AddListener(PlayClicked);
        options.onClick.AddListener(OptionsClicked);
        credits.onClick.AddListener(CreditsClicked);
    }


    private void PlayClicked()
    {
        SceneManager.LoadScene("Scenes/Fight", LoadSceneMode.Single);
    }

    private void OptionsClicked()
    {
        throw new System.NotImplementedException();
    }

    private void CreditsClicked()
    {
        throw new System.NotImplementedException();
    }
}